/* nekobee DSSI software synthesizer GUI
 *
 * Copyright (C) 2004 Sean Bolton
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include "knob.h"
#include "slider.h"

#include "nekobee.h"
#include "nekobee_ports.h"
#include "nekobee_synth.h"
#include "callbacks.h"
#include "interface.h"

GtkWidget *main_window;
GtkWidget *save_file_selection;

GtkWidget *osc1_waveform_pixmap;

GtkObject *tuning_adj;
GtkObject *polyphony_adj;
GtkWidget *monophonic_option_menu;
GtkWidget *glide_option_menu;
GtkObject *bendrange_adj;

GtkObject *voice_widget[XSYNTH_PORTS_COUNT];

#if GTK_CHECK_VERSION(2, 0, 0)
#define GTK20SIZEGROUP  GtkSizeGroup
#else
#define GTK20SIZEGROUP  gpointer
#endif

static void
create_voice_knob(GtkWidget *main_window, int index, GtkWidget *fixed,
                    gint column, gint row, const char *text)
{
    GtkWidget *knob;
    GtkObject *adjustment;

    adjustment = gtk_adjustment_new (0, 0, 10, 0.005, 1, 0);
    voice_widget[index] = adjustment;

    knob = gtk_knob_new (GTK_ADJUSTMENT (adjustment));

    // gjcp - FIXME horrible one-off to add the tuning knob
    if (index == XSYNTH_PORT_TUNING) GTK_KNOB(knob)->knobtype=1;

    gtk_widget_ref (knob);
    gtk_object_set_data_full (GTK_OBJECT (main_window), text, knob,
                              (GtkDestroyNotify) gtk_widget_unref);

    gtk_widget_show (knob);
    gtk_fixed_put (GTK_FIXED (fixed), knob, column, row);
}

static void
create_voice_slider(GtkWidget *main_window, int index, GtkWidget *fixed,
                    gint column, gint row, const char *text)
{
    GtkWidget *knob;
    GtkObject *adjustment;

    adjustment = gtk_adjustment_new (0, 0, 10, 0.005, 1, 0);
    voice_widget[index] = adjustment;

    knob = gtk_slider_new (GTK_ADJUSTMENT (adjustment));
    
    gtk_widget_ref (knob);
    gtk_object_set_data_full (GTK_OBJECT (main_window), text, knob,
                              (GtkDestroyNotify) gtk_widget_unref);
    gtk_widget_show (knob);
   	gtk_fixed_put (GTK_FIXED (fixed), knob, column, row);
}



void
create_main_window (const char *tag)
{
  GtkWidget *fixed;
  GtkStyle *bg_style;
  GdkPixbuf  *bg_buf;
  GdkPixmap  *bg_map = NULL; 

  int i;

  main_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_object_set_data (GTK_OBJECT (main_window), "main_window", main_window);
  gtk_window_set_title (GTK_WINDOW (main_window), tag);

  // gjcp - FIXME - put these somewhere in a header
  gtk_widget_set_size_request (main_window, 600, 108);
  gtk_window_set_default_size (GTK_WINDOW (main_window), 600, 108);
  gtk_window_set_resizable (GTK_WINDOW (main_window), FALSE);

  gtk_widget_realize(main_window);  /* window must be realized for create_*_pixmap() */

  // load the background up
  bg_buf = gdk_pixbuf_new_from_file(INSTALL_DIR"/bg.png", NULL );
  gdk_pixbuf_render_pixmap_and_mask(bg_buf, &bg_map, NULL, 255);
  bg_style = gtk_style_copy(gtk_widget_get_style(main_window));
  bg_style->bg_pixmap[GTK_STATE_NORMAL] = bg_map;
  gtk_widget_set_style(main_window, bg_style); 

  // rather than the table layout I'm going to use fixed
  fixed = gtk_fixed_new ();
  gtk_widget_show (fixed);
  gtk_container_add (GTK_CONTAINER (main_window), fixed);


  /* Patch Edit tab */
  for (i = 0; i < XSYNTH_PORTS_COUNT; i++) voice_widget[i] = NULL;

    // tuning can go from -12 to +12, so needs special poking
	create_voice_knob(main_window, XSYNTH_PORT_TUNING, fixed, 26, 46, "Tuning");

    GTK_ADJUSTMENT(voice_widget[XSYNTH_PORT_TUNING])->lower = -12.0f;
	GTK_ADJUSTMENT(voice_widget[XSYNTH_PORT_TUNING])->upper = 12.0f;
        
    gtk_adjustment_changed (GTK_ADJUSTMENT(voice_widget[XSYNTH_PORT_TUNING]));
	
	// wave switch also needs special poking
    create_voice_slider(main_window, XSYNTH_PORT_WAVEFORM, fixed, 116, 43, "Waveform");
	GTK_ADJUSTMENT(voice_widget[XSYNTH_PORT_WAVEFORM])->upper = 1.0f;
	GTK_ADJUSTMENT(voice_widget[XSYNTH_PORT_WAVEFORM])->step_increment = 1.0f;
	gtk_adjustment_changed (GTK_ADJUSTMENT(voice_widget[XSYNTH_PORT_WAVEFORM]));
   
    create_voice_knob(main_window, XSYNTH_PORT_CUTOFF, fixed, 170, 46, "Cutoff");
    create_voice_knob(main_window, XSYNTH_PORT_RESONANCE, fixed, 242, 46, "Resonance");
    create_voice_knob(main_window, XSYNTH_PORT_ENVMOD, fixed, 314, 46, "Envmod");
    create_voice_knob(main_window, XSYNTH_PORT_DECAY, fixed, 386, 46, "Decay");
    create_voice_knob(main_window, XSYNTH_PORT_ACCENT, fixed, 458, 46, "Accent");
    create_voice_knob(main_window, XSYNTH_PORT_VOLUME, fixed, 530, 46, "Volume");

    gtk_signal_connect(GTK_OBJECT(main_window), "destroy",
                       GTK_SIGNAL_FUNC(gtk_main_quit), NULL);
    gtk_signal_connect (GTK_OBJECT (main_window), "delete_event",
                        (GtkSignalFunc)on_delete_event_wrapper,
                        (gpointer)on_menu_quit_activate);
    

    /* connect patch edit widgets */
    gtk_signal_connect (GTK_OBJECT (voice_widget[XSYNTH_PORT_TUNING]),
                       "value_changed", GTK_SIGNAL_FUNC(on_voice_slider_change),
                       (gpointer)XSYNTH_PORT_TUNING);
    gtk_signal_connect (GTK_OBJECT (voice_widget[XSYNTH_PORT_WAVEFORM]),
                       "value_changed", GTK_SIGNAL_FUNC(on_voice_slider_change),
                       (gpointer)XSYNTH_PORT_WAVEFORM);
    gtk_signal_connect (GTK_OBJECT (voice_widget[XSYNTH_PORT_CUTOFF]),
                        "value_changed", GTK_SIGNAL_FUNC (on_voice_slider_change),
                        (gpointer)XSYNTH_PORT_CUTOFF);
    gtk_signal_connect (GTK_OBJECT (voice_widget[XSYNTH_PORT_RESONANCE]),
                        "value_changed", GTK_SIGNAL_FUNC(on_voice_slider_change),
                        (gpointer)XSYNTH_PORT_RESONANCE);

    gtk_signal_connect (GTK_OBJECT (voice_widget[XSYNTH_PORT_ENVMOD]),
                        "value_changed", GTK_SIGNAL_FUNC(on_voice_slider_change),
                        (gpointer)XSYNTH_PORT_ENVMOD);
    gtk_signal_connect (GTK_OBJECT (voice_widget[XSYNTH_PORT_DECAY]),
                        "value_changed", GTK_SIGNAL_FUNC(on_voice_slider_change),
                        (gpointer)XSYNTH_PORT_DECAY);
    gtk_signal_connect (GTK_OBJECT (voice_widget[XSYNTH_PORT_ACCENT]),
                        "value_changed", GTK_SIGNAL_FUNC(on_voice_slider_change),
                        (gpointer)XSYNTH_PORT_ACCENT);
    gtk_signal_connect (GTK_OBJECT (voice_widget[XSYNTH_PORT_VOLUME]),
                        "value_changed", GTK_SIGNAL_FUNC(on_voice_slider_change),
                        (gpointer)XSYNTH_PORT_VOLUME);
}

void
create_windows(const char *instance_tag)
{
    char tag[50];

    /* build a nice identifier string for the window titles */
    if (strlen(instance_tag) == 0) {
        strcpy(tag, "nekobee");
    } else if (strstr(instance_tag, "nekobee-DSSI") ||
               strstr(instance_tag, "nekobee-dssi")) {
        if (strlen(instance_tag) > 49) {
            snprintf(tag, 50, "...%s", instance_tag + strlen(instance_tag) - 46); /* hope the unique info is at the end */
        } else {
            strcpy(tag, instance_tag);
        }
    } else {
        if (strlen(instance_tag) > 37) {
            snprintf(tag, 50, "nekobee ...%s", instance_tag + strlen(instance_tag) - 34);
        } else {
            snprintf(tag, 50, "nekobee %s", instance_tag);
        }
    }

    create_main_window(tag);
}
